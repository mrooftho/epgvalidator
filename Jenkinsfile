pipeline {
  agent{
    node {
      label 'docker'
    }
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    disableConcurrentBuilds()
    skipDefaultCheckout()
    timestamps()
  }

  environment {
    platforms = 'x86_64'
    tag = 'development'
  }
  stages {
    stage("Building images"){
      steps {
        script {
          env.GIT_COMMIT = checkout(scm).GIT_COMMIT;
          env.SHORT_COMMIT = env.GIT_COMMIT.take(6)
        }
        buildImages()
      }
    }

    stage("Tagging images") {
      when {
        branch 'master'
      }
      steps{
        tagImages()
      }
    }

    stage("Pushing images") {
      when {
        branch 'master'
      }
      steps{
        pushImages()
      } 
    }
  }
  post {
    success {
      removeOldImages()
    }
    always {
      removeDanglingImages()
    }
  }
}
