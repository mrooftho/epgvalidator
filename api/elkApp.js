function elkApp(lab){
    //const elkService = require('../services/elkService')('localhost:9200',lab);
    const elkService = require('../services/elkService')(process.env.elastic,lab);
    
    const _ = require('lodash');
    return {
        submitOespSchedule
    };
    function submitOespSchedule(schedule) {
        return flattenOespEvent(schedule)
            .then((arr)=> {
                return elkService.submitDataToElastic(arr,'oesp-epg','oesp-enriched-epg-event');
            })
            .catch((e)=>{
                console.log('ELK','Error',e);
            });
    }

    function flattenOespEvent(schedule){
        return new Promise((resolve,reject)=>{
            let arr =  _.flattenDeep(schedule.channels.map(c => c.events));
        
            arr = arr.map(e => {
                return JSON.parse(
                    JSON.stringify(e, (key, value) => {
                            if (typeof value === 'function') {
                                return value();
                            }
                            return value;
                        })
                    );
            });
            resolve(arr);
        });
    }
}
module.exports = elkApp;