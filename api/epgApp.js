function epgApp(env){
    const _ = require('lodash');
    const moment = require('moment');

    const magOwner = `lgilabs-be-${env}-master`;
    const epgService = require('../services/epgService')(`http://epg-packager.${env}.appdev.io`);
    const oespService = require('../services/oespService')(process.env.oesp);
    const magService = require('../services/magService')('lgidev');
    
    return {
        getEOSSchedule,
        getGOSchedule,
        getMAGSchedule,
        linkMagAndGOSchedule
    }

    //Link GO OESP & MAG Schedule
    function linkMagAndGOSchedule(oesp,mag){
        oesp.channels.map(channel => {
            channel.events.map((event)=> {

                const magData = getMagDetailsForOESPEvent(event,channel.channelId,mag); 
                if (magData) { event.MAG = magData};
            
                event.hasMagData = () => { return event.MAG ? true : false  };
                event.magTagTouched = () => { return event.MAG ? event.MAG.tags.includes('touched') : false };
                event.magTagVrmOk = () => {return event.MAG ? event.MAG.tags.includes('mag-replay-vrm-ok'): false  };
                event.magTagIrdetoOk = () => {return event.MAG ? event.MAG.tags.includes('mag-replay-irdeto-ok') : false };
                event.hasRootId = () => {
                    if (!event.MAG) {
                        return false;
                    } else {
                        if (event.MAG.alternativeIdentifiers){
                            return event.MAG.alternativeIdentifiers.rootId ? true : false;
                        } else {
                            return false;
                        }
                        
                    }         
                };
                event.magRetreivedContent = () => {
                    //ContentRefData
                    if (!event.MAG){
                        return null;
                    } else {
                        return event.MAG.ContentRefData ? true : false;
                    }
                }
                event.magContentType = () => {
                    if (!event.MAG){
                        return null;
                    } else {
                        return event.MAG.ContentRefData ? event.MAG.ContentRefData.type : null;
                    }
                }
                event.magTagReplayTvEnabled = () => {
                    if (!event.MAG){
                        return false;
                    } else {
                        return event.MAG.ContentRefData ? event.MAG.ContentRefData.tags.includes('replayTvEnabled') : false;
                    }
                }
                return event;
            });
        });
        return oesp;
    }

    function getMagDetailsForOESPEvent(oespEvent,oespChannel,magData){
        if (!oespEvent.hasCridImi()) {return null};
        const magChannelId = oespChannel.split(':')[1];
        const magChannel = magData.channels.find(c => c.channelId === magChannelId);
        if (!magChannel) { return null };
        if (!magChannel.events) { return null };
        const magEvent = magChannel.events.find(e => e.alternativeIdentifiers.crid === oespEvent.formattedCridImi());
        return magEvent ? magEvent : null;
    }

    //MAG Scheduel section
    //"scopeContentRefs": [
    //    "lgilabs-be-lab5a-master:8e867efa7fc5dd40f4a4dbdf4dcc7e2cf406671cc4ab46dfc1b04a4fdcfaf7eb"
    //]
    function getMAGSchedule(services,days,includeContent=false){
        return magService.getToken().then((res)=>{
            const token = 'bearer ' + res.access_token;
            return Promise.all(services.map(sid => getMAGScheduleForService(token,sid,magOwner,days)));
        }).then((res)=>{
            return {channels: res};
        }).catch((ex)=>{
            console.log('Exception gathering MAG data',ex);
        });
    }

    function getMAGScheduleForService(token,service,owner,days,includeContent){
        return magService.getLiniearOffers(token,service,owner,days).then((pageOne)=>{
            const total = pageOne.meta.totalCount;
            const pages = Math.ceil(total/100);
            const arr = Array(pages).fill().map((e,i)=>i+1);
            return Promise.all(arr.map(i => magService.getLiniearOffers(token,service,owner,days,i)));
        }).then((pages)=>{
            let offers = [].concat.apply([], pages.map(p => p.offers));
            //return Promise.all(offers.map(o => getMAGContentForOffer(token,o)));
            return enrichMAGOffersWithContent(token,offers);
        }).then((offers)=>{
            return {channelId: service, events: offers};
        });
    }
    
    function enrichMAGOffersWithContent(token,offers){
        return new Promise((resolve,reject)=> {
            const enrichedOffers = [];
            var counter = -1;
            const PromisePool = require('es6-promise-pool')
            const promiseProducer =  () => {
                counter++;
                return counter == offers.length ? null : getMAGContentForOffer(token,offers[counter]);
            };
            const concurrency = 3;
            const pool = new PromisePool(promiseProducer, concurrency)
            pool.addEventListener('fulfilled', function (offerWithContent) {
                enrichedOffers.push(offerWithContent.data.result);
            });            
            // Wait for the pool to settle
            pool.start().then((res)  => {
                resolve(enrichedOffers);
            }).catch((error) => {
                console.log('Some promise rejected: ' + error.message)
            });
        });
    }

    function getMAGContentForOffer(token,offer){
        return magService.getContent(token ,offer.scopeContentRefs[0]).then((content) => {
            if (content){
                offer.ContentRefData = content.contents[0];
                return Promise.resolve(offer);
            } else {
                console.log('No content retreived');
                return Promise.resolve(offer);
            }
            
        });
    }

    //GO Schedule section
    function getGOSchedule(country,language,device){
        const days = [-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7];
        const periods = [1,2,3,4];
        const promises = [];
        days.forEach((d)=>{
            periods.forEach((p)=>{
                promises.push(oespService.getScheduleEvents(country,language,device,d,p));
            });
        });
        return Promise.all(promises)
            .then((res) => {
                return combineData(res);
            })
            .then((combined)=>{
                return enrichData(combined);
            })
            .catch((e)=>{
                return Promise.reject(e);
            });
    }

    //{channelId: '', events: []}
    function combineData(res){
        return new Promise((resolve,reject)=>{
            const rObj = [];
            res.forEach((obj)=>{
                obj.entries.forEach((entry)=>{
                    if (entry.l.length > 1){
                        const channel = rObj.find(e => e.channelId === entry.o);
                        if (channel){
                            channel.events = channel.events.concat(entry.l);
                        } else {
                            rObj.push({channelId: entry.o, events: entry.l});
                        }
                    }
                });
            });
            resolve(rObj);
        });
    }

    function enrichData(combinedData){
        return new Promise((resolve,reject)=>{

            Object.keys(combinedData).forEach((k)=>{
                combinedData[k].events.map((event)=>{
                    const startDate = new Date(0);
                    startDate.setUTCMilliseconds(event.s);
                    const endDate = new Date(0);
                    endDate.setUTCMilliseconds(event.e);

                    event.start = moment(startDate);
                    event.end = moment(endDate);
                    event.isPassed = () => { return event.start > moment() ? false : true };
                    event.isFuture = () => { return event.start > moment() ? true : false }; 
                    event.isLive = () => { return (event.start < moment() && event.end > moment()? true : false) };
                    event.channelId = () => { return  combinedData[k].channelId };
                    event.hasCridImi = () => {return event.i ? true : false };
                    event.formattedCridImi = () => { return event.i ? event.i.replace(/~~2F/g,'/') : null }
                    return event;
                });
                combinedData[k].current = () => {return combinedData[k].events.find(e => e.isLive())};
                combinedData[k].passedEvents = () => {return combinedData[k].events.filter(e => e.isPassed())};
                combinedData[k].futureEvents = () => {return combinedData[k].events.filter(e => e.isFuture())};
            });

            resolve({channels:combinedData});
        });
    }

    //###### EOS EPG DATA SECTION ########
    function getEOSSchedule(channelId,cpeid,country,lang){
        
        return epgService.getIndex(cpeid,country,lang)
            .then((epgindex) => {
                return lookUpChannelSegments(epgindex,channelId)
            }).then((segments) => {
                return retreiveDataForSegments(cpeid,country,lang,segments,channelId)
            }).catch((e)=>{
                return Promise.reject(e);
            });
    }

    function lookUpChannelSegments(index,channelId){
        return new Promise((resolve,reject)=>{
            index.entries.map((e) => {
                if (e.channelIds.includes(channelId)){
                    resolve(e.segments);
                }
            });
            reject(`${channelId} not found in index`);
        });
    }

    function retreiveDataForSegments(cpeid,country,lang,segments,channelId){
        return new Promise((resolve,reject)=> {
            let arr = segments.map(s => epgService.getSegment(cpeid,country,lang,s));
            Promise.all(arr).then((res)=>{
                //Concat to one
                let events = _.flatten(res.map((s) => {
                                    return s.entries;
                                }).map((e) => {
                                    return e[0].events
                                })).map(e => extendEventData(e));
                events.sort((a,b) => {
                    if (a.start > b.start){
                        return 1;
                    } else {
                        if (a.start === b.start) {
                            return 0;
                        } else {
                            return -1;
                        }
                    }
                });
                resolve({
                    channelId,
                    events,
                    current: () => {return events.find(e => e.isLive())},
                    passedEvents: () => {return events.filter(e => e.isPassed())},
                    futureEvents: () => {return events.filter(e => e.isFuture())}
                });


            }).catch((e) => {
                reject(`${e} unable to retreive segments`);
            })

        });
    }

    function extendEventData(event){
        event.start = moment.unix(event.startTime);
        event.end = moment.unix(event.endTime);
        event.isPassed = () => { return event.start > moment() ? false : true };
        event.isFuture = () => { return event.start > moment() ? true : false }; 
        event.isLive = () => { return (event.start < moment() && event.end > moment()? true : false) };
        return event;
    }
}

module.exports = epgApp;