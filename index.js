require('dotenv').config();
//Has CridImi
//Remove duplicate IDs
/*
const epgApp = require('./api/epgApp')(process.env.environment);
const elkApp = require('./api/elkApp')(process.env.environment);


epgApp.getGOSchedule('NL','nld','web').then((res)=>{
    console.log('elk upload');
    elkApp.submitOespSchedule(res).then((res)=>{
        console.log('done');
    });
});*/


const epgApp = require('./api/epgApp')(process.env.environment || 'lab5a');
const elkApp = require('./api/elkApp')(process.env.environment);

const data = {mag:null,oesp:null,consolidated:null};

epgApp.getGOSchedule('NL','nld','web').then((oespData)=>{
    data.oesp = oespData;
    const channels = oespData.channels.map(c => c.channelId.split(':')[1]);
    return epgApp.getMAGSchedule(channels,8);
}).then((magData)=>{
    data.mag = magData;
    data.consolidated = epgApp.linkMagAndGOSchedule(data.oesp,data.mag);
    return elkApp.submitOespSchedule(data.consolidated);
}).then((res)=>{
    console.log('done');
}).catch((ex)=>{
    console.log(ex);
}); 
//{channelId: '', events: []}
/*
epgApp.getEOSSchedule('0130','3C36E4-EOSSTB-003469679603','be','nl').then((epg)=>{
    console.log('done');
}).catch((err) => {
    console.log(`ERROR ${err}`);
});
*/

/*    JSON.stringify(res, replacer);

    function replacer(key, value) {
        // Filtering out properties
        if (typeof value === 'function') {
            return value();
        }
        return value;
    }*/


//MAG Qrys
//1. Get token
//      POST https://identity-reference.sequoia.piksel.com/oauth/token
//          Authorization -> Basic bGdpbGFicy1wbGF5Z3JvdW5kOnQ9VTleVVUsdFhWN0Al
//2. Get LinearOffers
//      GET https://metadata-lgidev.sequoia.piksel.com/data/offers?withType=linear&sort=availabilityStartAt&withAlternativeIdentifiers=serviceId:0062&owner=lgilabs-be-lab5a-master&perPage=100&count=type&withAvailabilityStartAt=2018-04-05T00:00:00.000Z/
//          Authorization -> bearer 28e5fc6d613785993fef477b7dea1d1135892264
//      Note: meta.totalCount holds number of results, to use for paging