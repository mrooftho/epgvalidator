function elkService(host,env) {
    const PromisePool = require('es6-promise-pool');
    const elasticBuilder = require('elasticsearch');

    const elasticClient = new elasticBuilder.Client({
        host: host,
        log: 'trace'
    });
    

    return {
        submitDataToElastic
    };


    function submitDataToElastic(arr, index,dataType) {
        return submitEventDataToElastic(arr, index, dataType);
    }

    function submitEventDataToElastic(arr, baseIndex, dataType) {
        return new Promise(function (resolve, reject) {            
            const moment = require('moment');
            const date = moment().format('YYYYMM');
            let monthylIndex = `${baseIndex}-${date}`;
            let referenceTime = moment();

            let counter = -1;
            const promiseStrack = function () {
                counter++;
                if (counter == arr.length) {
                    return null;
                } else {
                    return _submitData(arr[counter], referenceTime, monthylIndex, dataType);
                }
            }
            let pool = new PromisePool(promiseStrack, 10);

            pool.start()
                .then(function () {
                    resolve(true);
                });
        });
    }

    function _submitData(data, referenceTime, idx, datatype) {
        return new Promise(function (resolve, reject) {
            data.sample = referenceTime;
            data.enviroment = env;

            elasticClient.index({
                index: idx,
                type: datatype,
                body: data
            }, function (error, response) {
                if (error) {
                    console.log('_submitData', 'error', error);
                }
                resolve(true);
            });
        });
    }
}
module.exports = elkService;