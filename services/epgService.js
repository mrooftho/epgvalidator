const uuidv4 = require('uuid/v4');

function epgService(host) {
    const uri = host;
    return {
        getIndex,
        getSegment
    };

    function getIndex(cpeid, country, lang) {
        return httpRequest(`${host}/${country}/${lang}/events/segments/index`);
    }

    function getSegment(cpeid, country, lang, segment) {
        return httpRequest(`${host}/${country}/${lang}/events/segments/${segment}`);
    }

    function httpRequest(url, cpeid) {
        let http = require('request-promise');
        let opt = {
            method: 'GET',
            uri: url,
            timeout: 10000,
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': cpeid,
                'X-Request-Id': 'miro-' + uuidv4().replace(/-/g, '')
            }
        };
        return http(opt)
            .catch(error => {
                console.log({ error: error.error, message: error.message, name: error.name, uri: error.options.uri });
            });
    }
}

module.exports = epgService;