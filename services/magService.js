const uuidv4 = require('uuid/v4');

function magService(magUser) {
    const user = magUser;

    return {
        getToken,
        getLiniearOffers,
        getContent
    };

    
    async function getToken(auth = 'Basic bGdpbGFicy1wbGF5Z3JvdW5kOnQ9VTleVVUsdFhWN0Al'){
        return httpPostRequest(`https://identity-reference.sequoia.piksel.com/oauth/token`,'grant_type=client_credentials',{'Authorization': auth,'Content-Type':'application/x-www-form-urlencoded'});
    }

    async function getLiniearOffers(token,serviceId,owner,days,page = 1){
        const moment = require('moment');
        const since = moment().subtract(days,'days').format('YYYY-MM-DDThh:mm:ss');
        return httpRequest(`https://metadata-${user}.sequoia.piksel.com/data/offers?withType=linear&sort=availabilityStartAt&withAlternativeIdentifiers=serviceId:${serviceId}&owner=${owner}&perPage=100&count=type&page=${page}&withAvailabilityStartAt=${since}.000Z/`,{'Authorization': token});
    }

    async function getContent(token,contentRef){
        return httpRequest(`https://metadata-${user}.sequoia.piksel.com/data/contents/${contentRef}`,{'Authorization': token});
    }

    function httpPostRequest(url,body,addHeaders={}){
        let http = require('request-promise');
        let opt = {
            method: 'POST',
            uri: url,
            body: body,
            timeout: 120000,
            json: body ? true : false,
            headers: Object.assign(
                {
                    'Content-Type': 'application/json',
                    'X-Request-Id': 'miro-' + uuidv4().replace(/-/g, '')
                }
                ,addHeaders)
        };
        return http(opt)
            .catch(error => {
                console.log({ error: error.error, message: error.message, name: error.name, uri: error.options.uri });
            });
    }


    function httpRequest(url,addHeaders={}) {
        let http = require('request-promise');
        let opt = {
            method: 'GET',
            uri: url,
            timeout: 120000,
            json: true,
            headers: Object.assign(
                {
                    'Content-Type': 'application/json',
                    'X-Request-Id': 'miro-' + uuidv4().replace(/-/g, '')
                }
                ,addHeaders)
        };
        return http(opt)
            .catch(error => {
                console.log({ error: error.error, message: error.message, name: error.name, uri: error.options.uri });
            });
    }
}

module.exports = magService;
