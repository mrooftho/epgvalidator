const uuidv4 = require('uuid/v4');

function oespService(host) {
    const uri = host;

    return {
        getScheduleEvents
    };

    function getScheduleEvents(country, language, device, day, period) {
        const moment = require('moment');
        const date = moment().subtract(day, 'days').format('YYYYMMDD');
        return httpRequest(`${host}/${country}/${language}/${device}/programschedules/${date}/${period}`);

    }

    function httpRequest(url) {
        let http = require('request-promise');
        let opt = {
            method: 'GET',
            uri: url,
            timeout: 10000,
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'X-Request-Id': 'miro-' + uuidv4().replace(/-/g, '')
            }
        };
        return http(opt)
            .catch(error => {
                console.log({ error: error.error, message: error.message, name: error.name, uri: error.options.uri });
            });
    }
}

module.exports = oespService;
