const epgApp = require('./api/epgApp')(process.env.environment || 'lab5a');
const elkApp = require('./api/elkApp')(process.env.environment);

const data = {mag:null,oesp:null,consolidated:null};

epgApp.getGOSchedule('NL','nld','web').then((oespData)=>{
    data.oesp = oespData;
    const channels = oespData.channels.map(c => c.channelId.split(':')[1]);
    return epgApp.getMAGSchedule(channels,8);
}).then((magData)=>{
    data.mag = magData;
    data.consolidated = epgApp.linkMagAndGOSchedule(data.oesp,data.mag);
    return elkApp.submitOespSchedule(data.consolidated);
}).then((res)=>{
    console.log('done');
}).catch((ex)=>{
    console.log(ex);
}); 



/*
Promise.all([epgApp.getMAGSchedule(['0062'],8),epgApp.getGOSchedule('NL','nld','web')]).then((res)=> {
    const mag = res[0];
    const oesp = res[1];

    const consolidated = epgApp.linkMagAndGOSchedule(oesp,mag);
    console.log('done');
});
*/
/*
epgApp.getMAGSchedule(['0062'],8).then((res)=>{
    console.log('done');
});*/
/*
epgApp.getGOSchedule('NL','nld','web').then((res)=>{
    console.log('done');
});
*/
/*
const mag = require('./services/magService')('lgidev');

mag.getToken().then((res)=>{
    const token = 'bearer ' + res.access_token;
    return mag.getLiniearOffers(token,'0062','lgilabs-be-lab5a-master',8);
}).then((data)=>{
    console.log('hoppa');
});
*/